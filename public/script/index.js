"use strict";

const socket = io();
console.log("page loaded");
socket.emit("indexLoaded","new user on index.html");

const usernameEntry = document.getElementById("usernameEntry");
const usernameButton = document.getElementById("usernameButton");
const usernameInfo = document.getElementById("usernameInfo");
const chatEntry = document.getElementById("chatEntry");
const chatButton = document.getElementById("chatButton");
const chatHistory = document.getElementById("chatHistory");
const chatInputs = document.getElementsByClassName("chatInput");

var username;
usernameEntry.focus();

//USERNAME MANAGEMENT

usernameButton.addEventListener("click", setUsername);

usernameEntry.addEventListener("keypress", (e) => {
    if(e.keyCode===13){
        setUsername();
    }
});

function setUsername(){
    username = `${usernameEntry.value}`;
    if(username.length<=30&&username.length>2){
        usernameInfo.textContent += username;
        usernameEntry.value = "";
        chatInputs[0].className += " hidden";
        chatInputs[1].classList.remove("hidden");
        chatEntry.focus();
    }else{
        usernameEntry.focus();
        alert("votre speudo doit comprendre entre 3 et 30 caractères !");
    }
}

//MESSAGE MANAGEMENT

chatButton.addEventListener("click", sendMessage);

chatEntry.addEventListener("keypress", (e) => {
    if(e.keyCode===13){
        sendMessage();
    }
});

socket.on("chatUpdate",(message) => {
    let p = document.createElement("p");
    p.textContent+=message[1] + " : " + message[0];
    chatHistory.appendChild(p);
});

function sendMessage(){
    let message = `${chatEntry.value}`;
    if(message!==""){
        socket.emit("message", [message,username]);
    }
    chatEntry.value = "";
    chatEntry.focus();
    chatHistory.scrollTop = chatHistory.scrollHeight;
}
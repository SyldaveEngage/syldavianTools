"use strict";
function generateNavBar(current){
    
    const pages=["index","lavro","cowboy_party"];
    const navigationBar = document.getElementById("navigationBar");

    pages.forEach(page=>{

        let element = document.createElement("a");
        element.classList.add("navigation");
        element.setAttribute("href",`../pages/${page}.html`);
        element.innerText=page.charAt(0).toUpperCase() + page.slice(1).replace("_"," ");
        
        if(page===current){
            element.id="current";
        }
        navigationBar.appendChild(element);
    });

}
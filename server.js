"use strict";

const fs = require("fs");
const express = require("express");
const http = require("http");
const socketIO = require("socket.io");

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const PORT = process.env.PORT || 8080;
server.listen(PORT, () => console.log(`Listening on port ${PORT}`));

const serverChatHistory=[];

app.use(express.static("public"));
app.get("/",(req,res) => {
    res.status(200).sendFile(__dirname + "/public/pages/index.html");
});

io.on("connection", (socket) => {
	socket.on("indexLoaded", (data) => {
		console.log(data);
	});
	socket.on("message", ([message,username]) => {
		io.emit("chatUpdate",[message,username]);
		//addHistory([message,username]);
	});
});

